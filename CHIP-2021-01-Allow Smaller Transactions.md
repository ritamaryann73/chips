# CHIP 2021-01 Allow Transactions to be smaller in size

## Summary

This proposal updates the rule for minimum transation size to clean up technical debt which causes harm and while still protecting users on SPV nodes from being harmed by an attacker.

> Title: Allow Transactions to be smaller in size  
> Date: 2021-01  
> Type: Consensus  
> Status: Draft  
> Version: 0.3  
> Last edit: 2021-03-14  
> Owner: Tom Zander  
> History: https://gitlab.com/bitcoin.cash/chips  

## Motivation and Benefits

In June 2018 research was published surrounding a weakness in the interaction of Merkle-tree and Bitcoin's Merkle-block message, as mostly used by SPV wallets.

From [Leaf-Node weakness in Bitcoin Merkle Tree Design](https://bitslog.wordpress.com/2018/06/09/leaf-node-weakness-in-bitcoin-merkle-tree-design/):

> To summarize, the most space-efficient solution without a soft-fork would be hybrid: show inclusion proof of the rightmost TX id branch if the tree is not full, else **the coinbase** (showing mid-state and length) **if length is greater than 64 bytes**, or else show the full coinbase transaction.

The problem is that transactions of size exactly 64 bytes become a small but certain weakness that have potential of being exploited in order to defraud or steal from honest players.

In the November 2018 Protocol upgrade Bitcoin Cash reacted to this by making the transaction size have a minimum of 100 bytes. As a result this exploit is no longer possible on Bitcoin Cash.

This solution, however, created several problems.

The first and most hit is that a default coinbase transaction is smaller than 100 bytes. This means that new mining software creating a coinbase will often hit this limit which is an expensive mistake to make.

Alternative transaction types can be created which are similar smaller than 100 bytes, completely valid transactions.

These usecases all show that the entire ecosystem needs to take this limit into account, a limit unique to Bitcoin Cash. And a limit that is rather arbitrary.

Developers of libraries and software will hit this limit in many cases as well in their library testing software where the transactions created for testing are not about real money, and thus the scripts will not reflect reality. There are many places where automated testing systems had to be adjusted to account for this 100 bytes limit.

## Impact / Costs and Risks

The impact on the ecosystem is split into full nodes and others.

* Full nodes need to change the current test for minimum size of a transaction to now check the transaction is any size other than 64 bytes. This is a widening of the allowed transactions and therefore should be scheduled for a future protocol upgrade.

* All the players that are not full nodes do not have to change anything, as anything they generated before is still allowed.

This CHIP is mostly aimed at cleaning up technical debt. The result is less rules for most software. Additionally should this get solved in future on the p2p layer, then we can simply drop even the new rule.

## Proposal Description

To solve the unintended side-effects of the 100-bytes minimum, we simply remove this limit.

To avoid the weakness in merkle-blocks we introduce the rule that transactions shall not be 64 bytes in size.

## References:

Discussion on [bitcoincashresearch](https://bitcoincashresearch.org/t/154)

[Leaf-Node weakness in Bitcoin Merkle Tree Design](https://bitslog.wordpress.com/2018/06/09/leaf-node-weakness-in-bitcoin-merkle-tree-design/)

## Statements

**Freetrader, BCHN release manager.**  
> I reviewed this and would support activation of this CHIP (revision [75b97e22a3dc295de7373255025f38cd0911b866](https://gitlab.com/bitcoin.cash/chips/-/blob/75b97e22a3dc295de7373255025f38cd0911b866/CHIP-2021-01-Allow%20Smaller%20Transactions.md)) in the first network upgrade after May 2021. ([source](https://bitcoincashresearch.org/t/154/32))

**Benjamin Scherrey ([proteusguy](https://bitcoincashresearch.org/u/proteusguy)), ActorForth** 
> I’ve reviewed the CHIP and support it as written. My interest is as a development stakeholder. ([source](https://bitcoincashresearch.org/t/154/31))

**Andrew Stone, developer Bitcoin Unlimited**  
> Awemany (working as a BU dev) and I originally brought this problem to the BCH community’s attention. When ABC chose to limit at 100 bytes I pointed out this was idiocy way back before the original fork, and was ignored. So BU is happy to support a change that fixes yet-another-dumb-decision autocratically made during the ABC days. ([source](https://bitcoincashresearch.org/t/154/36))

## Copyright Notice

Copyright (C)  2021 Tom Zander  

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.

